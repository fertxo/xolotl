﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Configuration;

namespace Alux_Patch
{
    public partial class frmMain : Form
    {
        private string dbAlux = "";
        private string srcRestore = "";
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            dbAlux = ConfigurationManager.AppSettings["alux"];
            srcRestore = ConfigurationManager.AppSettings["path"];
        }

        private void _patchAlux()
        {
            string _xampp = "xampp-control";
            string _source = "C:\\xampp\\mysql\\data";
            string _target = srcRestore + "backup\\alux_" + DateTime.Now.ToString("yyMMdd");
            string _resMysql = srcRestore + "data";


            try
            {
                txtLog.AppendText(DateTime.Now.ToString() + " - " + _closeXAMPP(_xampp) + Environment.NewLine);
                Thread.Sleep(3000);
                txtLog.AppendText(DateTime.Now.ToString() + " - " + _closeXAMPP("mysqld") + Environment.NewLine);
                Thread.Sleep(3000);
                txtLog.AppendText(DateTime.Now.ToString() + " - " + _backupDB(_source, _target) + Environment.NewLine);
                Thread.Sleep(2000);
                txtLog.AppendText(DateTime.Now.ToString() + " - " + _restoreMySQL(_resMysql, _source) + Environment.NewLine);
                Thread.Sleep(2000);
                txtLog.AppendText(DateTime.Now.ToString() + " - " + _RestoreAlux(_source) + Environment.NewLine);
                Thread.Sleep(2000);
                txtLog.AppendText(DateTime.Now.ToString() + " - " + _deleteRestore(_source) + Environment.NewLine);
                Thread.Sleep(1000);
                txtLog.AppendText(DateTime.Now.ToString() + " - " + _openTask(_xampp) + Environment.NewLine);
                Thread.Sleep(2000);
                txtLog.AppendText(DateTime.Now.ToString() + " - " + _openTask("C:\\xampp\\mysql_start.bat", 0) + Environment.NewLine);
                Thread.Sleep(2000);
                txtLog.AppendText(DateTime.Now.ToString() + " - " + _openTask("c:\\xampp\\mysql\\bin\\mysql.exe -u root " + dbAlux, 0, " < " + srcRestore + "Procedures.sql") + Environment.NewLine);
                txtLog.AppendText(DateTime.Now.ToString() + " - Proceso finalizado, ya se puede usar Alux!!!" + Environment.NewLine);
                txtLog.AppendText("Gracias por su paciencia" + Environment.NewLine);
                lblAviso.Text = "Alux!!!";
            }
            catch (Exception ex)
            {
                txtLog.AppendText(DateTime.Now.ToString() + " - " + ex.Message + Environment.NewLine + "Favor de ponerse en contacto con junglaCODE para soporte");
            }
            
        }

        private string  _closeXAMPP(string _tsk)
        {
            try
            {
                foreach (Process proceso in Process.GetProcessesByName(_tsk))
                    proceso.Kill();
                return "Cerrando proceso " + _tsk ;
            }
            catch(Exception ex)
            {
                throw new Exception("Error - No se pudo cerrar " + _tsk + " " + ex.Message);
                //return "Error - No se pudo cerrar " + _tsk + " " + ex.Message;
            }
            
        }

        private string _backupDB(string presourcePath, string targetPath)
        {
            string sourcePath = presourcePath + "\\" + dbAlux;
            try
            {
                Directory.CreateDirectory(targetPath);
                foreach (string newPath in Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories))
                {
                    File.Copy(newPath, newPath.Replace(sourcePath, targetPath), true);
                }
                Directory.Move(presourcePath,presourcePath + "-bk");
                return "Respaldo generado";
            }
            catch (Exception ex)
            {
                throw new Exception("Error - No se pudo generar backup " + ex.Message);
            }
        }

        private string _RestoreAlux(string presourcePath)
        {
            string sourcePath = presourcePath + "-bk\\" + dbAlux ;
            string targetPath = presourcePath + "\\" + dbAlux;
            try
            {
                Directory.CreateDirectory(targetPath);
                foreach (string newPath in Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories))
                {
                    File.Copy(newPath, newPath.Replace(sourcePath, targetPath), true);
                }
                return "BD Alux restaurada";
            }
            catch (Exception ex)
            {
                throw new Exception("Error - Problemas con restauracion Alux " + ex.Message);
            }
        }

        private string _deleteRestore(string presourcePath)
        {
            string sourcePath = presourcePath + "-bk";
            try
            {
                Directory.Delete(sourcePath, true);
                return "Quitar respaldo";
            }
            catch (Exception ex)
            {
                throw new Exception("Error - Problemas en eliminar respaldo " + ex.Message);
            }
        }

        private string _restoreMySQL(string sourcePath, string targetPath)
        {
            try
            {
                Directory.CreateDirectory(targetPath);
                foreach (string dirPath in Directory.GetDirectories(sourcePath, "*", SearchOption.AllDirectories))
                {
                    Directory.CreateDirectory(dirPath.Replace(sourcePath, targetPath));
                }

                foreach (string newPath in Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories))
                {
                    File.Copy(newPath, newPath.Replace(sourcePath, targetPath), true);
                }
                return "Mysql Restaurado";
            }
            catch (Exception ex)
            {
                throw new Exception("Error - Problemas en restauracion " + ex.Message);
            }
        }

        private string _openTask(string _tsk, int _opt=1, string _params ="")
        {
            try
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();

                if (_opt == 1)
                {
                    process.StartInfo.FileName = "C:\\xampp\\" + _tsk + ".exe";
                    process.Start();
                }
                else
                {
                    ProcessStartInfo processInfo;

                    string filename = _tsk + _params;
                    processInfo = new ProcessStartInfo("cmd.exe", "/c " + filename);
                    processInfo.CreateNoWindow = true;
                    processInfo.UseShellExecute = false;

                    process = Process.Start(processInfo);
                }
                return "Iniciando " + _tsk ;
            }
            catch (Exception ex)
            {
                throw new Exception("Error - No se pudo iniciar " + _tsk + " " + ex.Message);
            }
        }

        private void btnRestaurar_Click(object sender, EventArgs e)
        {
            lblAviso.Text = "Trabajando...";
            btnRestaurar.Enabled = false;
            _patchAlux();
        }
    }
}
