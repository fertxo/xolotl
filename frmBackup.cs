﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Alux_Patch
{
    public partial class frmBackup : Form
    {
        private string dbAlux = "";
        public frmBackup()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSource_Click(object sender, EventArgs e)
        {
            string dummyFileName = "Guardar aquí";
            SaveFileDialog sf = new SaveFileDialog();
            sf.FileName = dummyFileName;
            if (sf.ShowDialog() == DialogResult.OK)
            {
                string savePath = Path.GetDirectoryName(sf.FileName);
                txtPath.Text = savePath;
                btnRestaurar.Enabled = txtPath.Text.Length > 0 ? true : false;
            }
        }

        private void btnRestaurar_Click(object sender, EventArgs e)
        {
            lblAviso.Text = "Respaldando...";
            dbAlux = ConfigurationManager.AppSettings["alux"];
            btnRestaurar.Enabled = false;
            _backupAlux();
        }

        private void _backupAlux()
        {
            try {
                string _pathAlux = txtPath.Text + "\\alux" + DateTime.Now.ToString("yyMMddHHmmss") + ".sql";
                txtLog.AppendText(DateTime.Now.ToString() + " - " + _createBackup("c:\\xampp\\mysql\\bin\\mysqldump -u root " + dbAlux,  " > " + _pathAlux) + Environment.NewLine);
                Thread.Sleep(2000);
                txtLog.AppendText(DateTime.Now.ToString() + " - Respaldo creado!!!" + Environment.NewLine);
                txtLog.AppendText("Gracias por su paciencia" + Environment.NewLine);
                lblAviso.Text = "Respaldar tu información de Alux";
                btnRestaurar.Enabled = true;
            }
            catch (Exception ex)
            {
                txtLog.AppendText(DateTime.Now.ToString() + " - " + ex.Message + Environment.NewLine + "Favor de ponerse en contacto con junglaCODE para soporte");
            }
        }

        private string _createBackup(string _tsk, string _params = "")
        {
            try
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();

                ProcessStartInfo processInfo;

                string filename = _tsk + _params;
                processInfo = new ProcessStartInfo("cmd.exe", "/c " + filename);
                processInfo.CreateNoWindow = true;
                processInfo.UseShellExecute = false;

                process = Process.Start(processInfo);


                return "Respaldo creado!";
            }
            catch (Exception ex)
            {
                throw new Exception("Error - No se pudo crear el respaldo " + ex.Message);
            }
        }
    }
}
