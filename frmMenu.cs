﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Alux_Patch
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnRestaurar_Click(object sender, EventArgs e)
        {
            frmBackup _bk = new frmBackup();
            _bk.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmMain _mn = new frmMain();
            _mn.ShowDialog();
        }
    }
}
